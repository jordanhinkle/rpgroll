<?php
// RPGRoll/DiceRoller.php

/**
 * This parses command line input and creates RPGRoll\Dice and RPGRoll\Roller
 * objects from valid input. Also performs sanity checks on those objects
 * before attempting to execute a dice roll.
 * 
 * @author Jordan Hinkle <jordan.hinkle@gmail.com>
 */

namespace RPGRoll;

require('Roller.php');
require('Dice.php');

class DiceRoller {
    
    /**
     * Format of valid argument/input
     */
    const ROLL_REGEX = '/^\d{1,4}d\d{1,3}$/i';
    
    /**
     * Number of command line arguments required
     */
    const NUM_CLI_ARGS = 2;
    
    /**
     * Delimeter used to parse command line argument.
     */
    const ROLL_DELIMETER = 'd';
    
    /**
     * Index of the number of dice value in the exploded _inputArgumentString array.
     */
    const NUMBER_DICE_INDEX = 0;
    
    /**
     * Index of the number of sides value in the exploded _inputArgumentString array.
     */
    const NUMBER_SIDES_INDEX = 1;
    
    /**
     * An array of all the command like arguments given.
     * @var array
     */
    private $_inputArgumentArray;
    
    /**
     * A single dice object representing an actual die.
     * @var RPGRoll\Dice
     */
    private $_dice;
    
    /**
     * A Roller object used to roll dice.
     * @var RPGRoll\Roller
     */
    private $_roller;
    
    /**
     * Constructs an object using an array, $input, as its only parameter. Also
     * creates Dice and Roller objects for later use.
     * 
     * @param array $input, Array of arguments passed from the command line
     */
    function __construct($input) {
        $this->_inputArgumentArray = $input;
        $this->_dice               = new Dice();
        $this->_roller             = new Roller();
    }
    
    /**
     * Validates the input arguments provided and echos
     * descriptive error messages.
     * 
     * @return boolean TRUE if input format and value is valid, FALSE otherwise.
     */
    public function parseInput() {
        
        /*
         *  Check if the number of arguments given is correct.
         */
        $numOfArgumentsGiven = count($this->_inputArgumentArray);
        if ($numOfArgumentsGiven !== self::NUM_CLI_ARGS) {
            echo "Incorrect number of arguments. Expected: ".self::NUM_CLI_ARGS." given: ".$numOfArgumentsGiven."\n";
            return false;
        }
        
        $inputArgumentString = $this->_inputArgumentArray[1];
        
        /*
         *  Check if the input is in the correct format by using regex
         */
        if (!preg_match(self::ROLL_REGEX, $inputArgumentString)) {
            echo "Input argument is not in a valid format. Expected to match: ".self::ROLL_REGEX."\n";
            return false;
        }
        
        /*
         * Set the delimiter to lowercase then explode the string into an array
         */
        $inputArgumentString    = strtolower($inputArgumentString);
        $diceCountAndSidesArray = explode(self::ROLL_DELIMETER, $inputArgumentString);
        
        /*
         *  Try setting the numberOfSides property on the _dice object
         */
        $numberOfDiceGiven = $diceCountAndSidesArray[self::NUMBER_DICE_INDEX];
        if (!$this->_roller->setNumberOfDice($numberOfDiceGiven)) {
            return false;
        }
        
        /*
         *  Try setting the numberOfSides property on the _dice object
         */
        $numberOfSidesGiven = $diceCountAndSidesArray[self::NUMBER_SIDES_INDEX];
        if (!$this->_dice->setNumberOfSides($numberOfSidesGiven)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Checks to make sure $_roller and $_dice aren't null then returns
     * the result of passing $_dice to $_roller->roll().
     * 
     * @return int, A random number
     */
    public function rollDice() {
        if ($this->_roller === NULL || $this->_roller->getNumberOfDice() === NULL) {
            return "";
        }
        
        if ($this->_dice === NULL || $this->_dice->getNumberOfSides() === NULL) {
            return "";
        }
        
        return $this->_roller->roll($this->_dice);
    }
    
}

?>