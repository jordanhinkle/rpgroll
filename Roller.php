<?php
// RPGRoll/Roller.php

/**
 *  This represents an object with a integer representing 
 *  a specific number of Dice objects. The number of dice 
 *  must be between MIN_NUMBER_OF_DICE and
 *  MAX_NUMBER_OF_DICE, inclusive. Also included is a function
 *  that will simulate rolling a given dice object $_numberOfDice times.
 *
 * @author Jordan Hinkle <jordan.hinkle@gmail.com>
 */

namespace RPGRoll;

class Roller {
    
    /**
     * Minimum number of dice required to execute a roll.
     */
    const MIN_NUMBER_OF_DICE = 1;
    
    /**
     * Maximum number of dice that can be 'rolled'
     */
    const MAX_NUMBER_OF_DICE = 1000;
    
    /**
     * Number of dice to roll.
     *
     * Potential values are between MIN_NUMBER_OF_DICE 
     * and MAX_NUMBER_OF_DICE, inclusive
     * 
     * @var int
     */
    private $_numberOfDice;
    
    /**
     *  Empty constructor. Sets _numOfDice to MIN_NUMBER_OF_DICE
     */
    public function __construct() {
        $this->_numberOfDice = self::MIN_NUMBER_OF_DICE;
    }
    
    /**
     * Returns the value of $_numberOfDice
     * 
     * @return int $_numberOfDice
     */
    public function getNumberOfDice() {
        return $this->_numberOfDice;
    }
    
    /**
     * Given a number this function will first check if it is within
     * an acceptable range then sets the $_numberOfDice if it is.
     * 
     * @param string $numDice, Number of dice the user wishes to roll.
     * @return boolean, TRUE if $_numberOfDice could be set, FALSE otherwise.
     */
    public function setNumberOfDice($numDice) {
        $numDice = intval($numDice);
        if ($numDice < self::MIN_NUMBER_OF_DICE || $numDice > self::MAX_NUMBER_OF_DICE) {
            echo "Invalid number of dice. Expected between ".self::MIN_NUMBER_OF_DICE." and ".self::MAX_NUMBER_OF_DICE.". Given: ".$numDice."\n";
            return false;
        }
        $this->_numberOfDice = $numDice;
        return true;
    }
    
    /**
     * Given the number of dice and number of sides the dice 
     * have this function calculates the maximum and minimum 
     * numbers that can be rolled then returns a random 
     * number between those two values.
     * 
     * @return int, random number
     */
    public function roll($dice) {
        $max = $dice->getNumberOfSides() * $this->_numberOfDice;
        $min = $this->_numberOfDice;
        return rand($min, $max);
    }
}
?>