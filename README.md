# RPGRoll

RPGRoll is command line php script that simulates rolling dice. It accepts a single argument formatted like XdY where X is between 1 and 1000 and Y is between 2 and 100.

### Execution
`$ php roll.php 1d6`