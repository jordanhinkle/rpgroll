<?php
// RPGRoll/roll.php

/**
 *  Short wrapper class that creates a RPGRoll\DiceRoller object,
 *  executes a dice and echos the result. If the command line input
 *  is invalid the script stops execution and exits with a status code of 1.
 *
 * @author Jordan Hinkle <jordan.hinkle@gmail.com>
 */

require('DiceRoller.php');

use RPGRoll\DiceRoller;

/*
 * Create RPGRoll\DiceRoller object passing the entire command line
 * argument array. Parses the array and exits prematurely if
 * the input provided is invalid.
 */
$diceRoller = new DiceRoller($argv);
if (!$diceRoller->parseInput()) {
    exit(1);
}

/*
 * Executes a dice roll and stores the result into $rollResult. 
 * If successful an integer is returned otherwise an error message is shown.
 */
$rollResult = function() use ($diceRoller) {
    $roll = $diceRoller->rollDice();
    if ($roll === NULL) {
        return "Unable to execute rollDice().\n";
    }
    return $roll."\n";
};

echo $rollResult();

?>