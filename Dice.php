<?php
// RPGRoll/Dice.php

/**
 *  This represents an object with a specific number of sides. The
 *  number of sides must be between MIN_NUMBER_SIDES and
 *  MAX_NUMBER_SIDES, inclusive.
 *
 * @author Jordan Hinkle <jordan.hinkle@gmail.com>
 */

namespace RPGRoll;

class Dice {
    
    /**
     * Minimum number of sides a die must have.
     */
    const MIN_NUMBER_SIDES = 2;
    
    /**
     * Maximum number of sides one die can have.
     */
    const MAX_NUMBER_SIDES = 100;
    
    /**
     * Number of sides the dice object has.
     *
     * Potential values are between MIN_NUMBER_SIDES 
     * and MAX_NUMBER_SIDES, inclusive
     * 
     * @var int
     */
    private $_numberOfSides;
    
    /**
     * 
     */
    public function __construct() {
        $this->_numberOfSides = self::MIN_NUMBER_SIDES;
    }
    
    /**
     * Returns the value of $_numberOfSides
     * 
     * @return int $numberOfSides
     */
    public function getNumberOfSides() {
        return $this->_numberOfSides;
    }
    
    /**
     * Given a number this function will first check if it is within
     * an acceptable range then sets the $_numberOfSides if it is.
     * 
     * @param   string $numSides, Number of sides on the die.
     * @return  boolean, TRUE if $numberOfSides could be set, FALSE otherwise.
     */
    public function setNumberOfSides($numSides) {
        $numSides = intval($numSides);
        if ($numSides < self::MIN_NUMBER_SIDES || $numSides > self::MAX_NUMBER_SIDES) {
            echo "Invalid number of sides. Expected between ".self::MIN_NUMBER_SIDES." and ".self::MAX_NUMBER_SIDES.". Given: ".$numSides."\n";
            return false;
        }
        
        $this->_numberOfSides = $numSides;
        return true;
    }
}

?>